import './App.css';
import Clock from "./clock/clock";
import { useState } from 'react';

function App() {

  const backgrounds = [
  "https://icon2.cleanpng.com/20180217/rdw/kisspng-social-media-marketing-time-management-clock-clock-without-hands-5a87ba8f3374d3.4807223315188445592108.jpg",
  "https://p.kindpng.com/picc/s/64-649986_clock-no-hands-png-clipart-roman-numerals-clock.png",
  "https://st2.depositphotos.com/1431107/9238/i/950/depositphotos_92386560-stock-photo-blank-clock-dial-without-hands.jpg",
  "https://toppng.com/uploads/preview/clock-with-no-hands-11549789952ffvqx0vcir.png"
  ];

  const minimum = 0;
  const maximum = backgrounds.length-1;

  const randomNumber = () => Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
  const [background,setBackground] = useState(randomNumber());

  return (
    <div className="App">
      <Clock background={backgrounds[background]} />
      <button className="button" onClick={() => { setBackground(randomNumber())}}>Switch background</button>
    </div>
  );
}

export default App;
 