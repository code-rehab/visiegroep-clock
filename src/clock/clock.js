import PointerHour from "./pointer-hour";
import PointerMinutes from "./pointer-minutes";
import PointerSeconds from "./pointer-seconds";

function Clock(props) {

  const backgroundImage = "url('"+props.background+"')";

  return ( 
    <div className="clock" style={{backgroundImage}}> 
      <PointerHour/>
      <PointerMinutes/>
      <PointerSeconds/>
    </div>
  );
}

export default Clock;
 